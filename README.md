# MHDLM



## O aplikácií

Naša aplikácia je schopná vypísať odchody a príchody autobusov, podľa vopred zadaných parametrov.
Aplikácia obsahuje textové polia pre začiatočnú a končiacu zastávku, tlačidlá na nastavenie dátumu a času odchodu
a prepínač na zvolenie či sa jedná o príchod alebo odchod. Aplikácia funguje aj bez prístupu na internet (offline)
čo zabezpečuje lokálne uloženie údajov. Medzi ďalšie funkcionality patrí výmena zastávok jedným kliknutím
a uloženie 5 posledných navštívených spojov, pre rýchlejšie vyhľadávanie.

Problém môže vzniknúť ak dopravný podnik v Liptovskom Mikuláši zmení cestovný poriadok. Tento problém by sa dal vyriešiť ak by dopravný podnik poskytol API pre prístup k ich databáze. Prípadne vytvoriť ďalší program, ktorý dokáže z PDF súborov extrahovať potrebné dáta a vložiť ich do CSV súboru tak aby ho dokázala aplikácia prečítať a pri prístupe na internet sa aplikácia aktualizuje.

## Vzhľad aplikácie
![img_1.png](img_1.png) ![img_2.png](img_2.png)

## Inštalácia aplikácie
Aplikáciu je možné nainštalovať pomocou odkazu nižšie:

[![Download APK](https://img.shields.io/badge/download-APK-blue?style=for-the-badge&logo=android)](https://gitlab.com/Dragovit/mhdlm/-/blob/main/app/release/MHDLM.apk)

## Autor
Pavol Tomčík

## Verzia
v1.0