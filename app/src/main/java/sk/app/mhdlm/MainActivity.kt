package sk.app.mhdlm


import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.rememberNavController
import sk.app.mhdlm.screens.navigation.Navigation
import sk.app.mhdlm.ui.theme.MHDLMTheme

/**
 * Application for searching departures and arrivals MHD buses in Liptovský Mikuláš.
 * Features:
 *  -   App is working without internet
 *  -   Fast search according to last searched bus links
 *  -   Switching start and final bus stop
 *  -   App support Slovak and English language according to system language
 *
 * @author Pavol Tomčik
 */

class MainActivity : ComponentActivity() {

    @SuppressLint("SourceLockedOrientationActivity")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        installSplashScreen()
        setContent {
            MHDLMTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Navigation().SetUp(
                        navController = rememberNavController(),
                        sharedViewModel = viewModel())
                }
            }
        }
    }
}
