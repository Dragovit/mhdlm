package sk.app.mhdlm.data

import android.app.Application
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.AndroidViewModel
import kotlinx.coroutines.delay
import java.time.LocalTime
import java.time.format.DateTimeFormatter

/**
 * Storage for shared data between screens.
 * Contain functions for update and save data even app is closed.
 * Contain function for time refresh every second.
 *
 * @author Pavol Tomčik
 */

class SharedViewModel(application: Application) : AndroidViewModel(application) {
    var startBusStop by mutableStateOf("")
    var finalBusStop by mutableStateOf("")
    var date by mutableStateOf("")
    var time by mutableStateOf("")
    var isArrival by mutableStateOf(false)
    var departureDataList by mutableStateOf<List<DepartureData>>(emptyList())
    var currentTime by mutableStateOf("")
    var currentDate by mutableStateOf("")
    val lastBusStopsList = mutableListOf<String>()

    private val sharedPreferences = application.getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE)

    init {
        val savedLastBusStop = sharedPreferences.getString("lastBusStops", "") ?: ""
        lastBusStopsList.addAll(savedLastBusStop.split(";"))
    }

    fun updateLastBusStopData(data: String) {
        val busStopsList = data.split("\n")
        if (busStopsList.size == 2) {
            startBusStop = busStopsList[0]
            finalBusStop = busStopsList[1]
        }
        saveLastBusStops()
    }

    fun saveLastBusStops() {
        val newStartBusStop = startBusStop
        val newFinalBusStop = finalBusStop

        if (newStartBusStop.isNotEmpty() && newFinalBusStop.isNotEmpty()) {
            val newText = "$newStartBusStop\n$newFinalBusStop"
            if (lastBusStopsList.contains(newText)) {
                lastBusStopsList.remove(newText)
            }
            else if (lastBusStopsList.contains("")) {
                lastBusStopsList.remove("")
            }
            lastBusStopsList.add(0, newText)
            if (lastBusStopsList.size > 5) {
                lastBusStopsList.removeAt(5)
            }
            val savedLastBusStop = lastBusStopsList.joinToString(";")
            sharedPreferences.edit().putString("lastBusStops", savedLastBusStop).apply()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    suspend fun timeRefresh() {
        while (true) {
            val formattedTime = DateTimeFormatter
                .ofPattern("HH:mm")
                .format(LocalTime.now())
            currentTime = formattedTime
            delay(1000)
        }
    }

}