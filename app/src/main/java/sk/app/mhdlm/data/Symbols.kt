package sk.app.mhdlm.data

/**
 * Symbols in CSV file
 *
 * @author Pavol Tomčik
 */

enum class Symbols(val value: String) {
    SYMBOL_WORK_DAYS("p"),  // operate in work days
    SYMBOL_HOLIDAYS("+"),   // operate in holidays
    SYMBOL_6("6"),          // operate in saturday
    SYMBOL_10("10"),        // not operate in range of days
    SYMBOL_11("11"),        // operate in range of days
    SYMBOL_20("20"),        // not operate in specific date
    SYMBOL_27("27"),        // not operate in range of days
    SYMBOL_38("38"),        // not operate in specific date
    SYMBOL_47("47"),        // not operate in specific dates
    SYMBOL_56("56"),        // not operate in range of days
    SYMBOL_97("97"),        // not operate in range of days

    // Combination of symbols
    SYMBOL_6_HOLIDAYS("6+"),
    SYMBOL_6_20("620"),
    SYMBOL_6_HOLIDAYS_27("6+27"),
    SYMBOL_6_HOLIDAYS_38("6+38"),
    SYMBOL_6_HOLIDAYS_47("6+47"),
    SYMBOL_6_HOLIDAYS_56("6+56"),

    SYMBOL_WORK_DAYS_HOLIDAY("p+"),
    SYMBOL_WORK_DAYS_6("p6"),
    SYMBOL_WORK_DAYS_6_20("p620"),
    SYMBOL_WORK_DAYS_10("p10"),
    SYMBOL_WORK_DAYS_11("p11"),
    SYMBOL_WORK_DAYS_27("p27"),
    SYMBOL_WORK_DAYS_56("p56"),
}