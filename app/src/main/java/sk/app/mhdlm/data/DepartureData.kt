package sk.app.mhdlm.data

/**
 * Data read form CSV file
 *
 * @author Pavol Tomčik
 */

data class DepartureData(
    val busStopName: String,
    val timeList: List<String>,
    val busNumbersList: List<String>,
    val symbolsList: List<String>,
    val requestStop: String
)
