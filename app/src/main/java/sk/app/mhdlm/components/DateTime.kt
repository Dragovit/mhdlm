package sk.app.mhdlm.components

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import com.vanpra.composematerialdialogs.MaterialDialog
import com.vanpra.composematerialdialogs.datetime.date.DatePickerDefaults
import com.vanpra.composematerialdialogs.datetime.date.datepicker
import com.vanpra.composematerialdialogs.datetime.time.TimePickerDefaults
import com.vanpra.composematerialdialogs.datetime.time.timepicker
import com.vanpra.composematerialdialogs.rememberMaterialDialogState
import sk.app.mhdlm.R
import sk.app.mhdlm.data.SharedViewModel
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter

/**
 * DateTime function shows buttons with date and time dialog picker.
 * After date or time pick shows date or time on the screen.
 * If user do not pick any date or time then current date and time will shown.
 *
 * @author Pavol Tomčik
 */

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun DateTime(
    sharedViewModel: SharedViewModel
) {
    var date by remember { mutableStateOf(LocalDate.now()) }
    var time by remember { mutableStateOf(LocalTime.now()) }

    // Formatting date to dd.MM.yyyy
    val formattedDate by remember { derivedStateOf {
        DateTimeFormatter
            .ofPattern("dd.MM.yyyy")
            .format(date)}
    }
    sharedViewModel.date = formattedDate

    // Formatting current date to dd.MM.yyyy
    val currentDate by remember { derivedStateOf {
        DateTimeFormatter
            .ofPattern("dd.MM.yyyy")
            .format(LocalDate.now())}
    }
    sharedViewModel.currentDate = currentDate

    // Formatting time to HH:mm
    val formattedTime by remember {
        derivedStateOf {
            DateTimeFormatter
                .ofPattern("HH:mm")
                .format(time)
        }
    }
    sharedViewModel.time = formattedTime

    // Formatting current time to HH:mm
    val currentTime by remember {
        derivedStateOf {
            DateTimeFormatter
                .ofPattern("HH:mm")
                .format(LocalTime.now())
        }
    }
    sharedViewModel.currentTime = currentTime

    val dateDialogState = rememberMaterialDialogState()
    val timeDialogState = rememberMaterialDialogState()

    Column {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Button(
                onClick = { dateDialogState.show() },
                modifier = Modifier.width(100.dp)
            ) {
                Text(text = stringResource(R.string.date))
            }
            Button(
                onClick = { timeDialogState.show() },
                modifier = Modifier.width(100.dp)
            ) {
                Text(text = stringResource(R.string.time))
            }
        }

        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {

            Text(text = formattedDate, Modifier.width(100.dp), textAlign = TextAlign.Center)
            Text(text = formattedTime, Modifier.width(100.dp), textAlign = TextAlign.Center)
        }

        // Date dialog
        MaterialDialog(
            dialogState = dateDialogState,
            properties = DialogProperties(dismissOnClickOutside = true),
            backgroundColor = MaterialTheme.colorScheme.background,

            buttons = {
                positiveButton(text = stringResource(R.string.ok), textStyle = TextStyle().copy(
                    MaterialTheme.colorScheme.primary))
                negativeButton(text =  stringResource(R.string.cancel), textStyle = TextStyle().copy(
                    MaterialTheme.colorScheme.primary))
            }
        ){
            datepicker(
                initialDate = LocalDate.now(),
                title = stringResource(R.string.dateTitle),
                colors = DatePickerDefaults.colors(
                    headerBackgroundColor = MaterialTheme.colorScheme.primary,
                    headerTextColor = MaterialTheme.colorScheme.background,
                    calendarHeaderTextColor = MaterialTheme.colorScheme.primary,
                    dateActiveBackgroundColor = MaterialTheme.colorScheme.primary,
                    dateActiveTextColor = MaterialTheme.colorScheme.background,
                    dateInactiveTextColor = MaterialTheme.colorScheme.onBackground
                )
            ) {
                date = it
            }
        }

        // Time dialog
        MaterialDialog(
            dialogState = timeDialogState,
            properties = DialogProperties(dismissOnClickOutside = true),
            backgroundColor = MaterialTheme.colorScheme.background,

            buttons = {
                positiveButton(text = stringResource(R.string.ok), textStyle = TextStyle().copy(
                    MaterialTheme.colorScheme.primary))
                negativeButton(text = stringResource(R.string.cancel), textStyle = TextStyle().copy(
                    MaterialTheme.colorScheme.primary))
            }
        ){
            timepicker(
                initialTime = LocalTime.now(),
                title = stringResource(R.string.timeTitle),
                is24HourClock = true,
                colors = TimePickerDefaults.colors(
                    headerTextColor = MaterialTheme.colorScheme.primary,
                    activeBackgroundColor = MaterialTheme.colorScheme.primary,
                    activeTextColor = MaterialTheme.colorScheme.background,
                    inactiveTextColor = MaterialTheme.colorScheme.onBackground,
                    selectorColor = MaterialTheme.colorScheme.primary,
                    selectorTextColor = MaterialTheme.colorScheme.background
                )
            ) {
                time = it

            }
        }
    }
}
