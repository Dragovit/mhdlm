package sk.app.mhdlm.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import sk.app.mhdlm.R

/**
 * Arrival switch is switch for change searching from departures to arrival time.
 *
 * @author Pavol Tomčik
 */

@Composable
fun ArrivalSwitch(
    isArrival: Boolean,
    onCheckedChange: (Boolean) -> Unit
)
{
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 16.dp),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = if (isArrival)
                stringResource(R.string.arrival) else
                    stringResource(R.string.departure),
            modifier = Modifier.padding(top = 12.dp)
        )
        Switch(
            checked = isArrival,
            onCheckedChange = onCheckedChange,
            colors = SwitchDefaults.colors(
                uncheckedBorderColor = MaterialTheme.colorScheme.primary,
                uncheckedThumbColor = MaterialTheme.colorScheme.primary,
                uncheckedTrackColor = MaterialTheme.colorScheme.background
            ),
            modifier = Modifier.padding(end = 16.dp))

    }
}
