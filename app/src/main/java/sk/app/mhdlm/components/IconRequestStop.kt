package sk.app.mhdlm.components

import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import sk.app.mhdlm.R
import sk.app.mhdlm.data.DepartureData

/**
 * Icon for bus stops which are on request.
 * In the CSV file are bus stops on request mark by "X".
 *
 * @author Pavol Tomčik
 */

@Composable
fun IconRequestStop(
    busStopName: String,
    departureDataList: List<DepartureData>
) {
    val list = departureDataList.find { it.busStopName == busStopName }
    val index = departureDataList.indexOf(list)

    if (departureDataList[index].requestStop == "X") {
        Icon(
            painter = painterResource(R.drawable.hail),
            contentDescription = stringResource(R.string.requestStop)
        )
    }
}