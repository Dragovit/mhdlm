package sk.app.mhdlm.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import sk.app.mhdlm.data.DepartureData
import sk.app.mhdlm.data.SharedViewModel
import sk.app.mhdlm.screens.navigation.Screen

/**
 * Last links card are for showing last searched bus links.
 * Maximum number of cards is 5. It is possible to click on that card and show search results.
 *
 * @author Pavol Tomčik
 */

@Composable
fun LastLinksCards(
    navController: NavController,
    sharedViewModel: SharedViewModel,
    departureDataList: List<DepartureData>,
    isArrival: Boolean
) {
    if (sharedViewModel.lastBusStopsList[0] != "") {
        LazyColumn {
            items(sharedViewModel.lastBusStopsList.size) {
                val favorite = sharedViewModel.lastBusStopsList[it]
                Card(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(8.dp)
                ) {
                    Text(
                        text = favorite,
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(8.dp)
                            .clickable {
                                sharedViewModel.isArrival = isArrival
                                sharedViewModel.updateLastBusStopData(favorite)
                                sharedViewModel.departureDataList = departureDataList
                                navController.navigate(route = Screen.Results.route)
                            }
                    )
                }
            }
        }
    }
}