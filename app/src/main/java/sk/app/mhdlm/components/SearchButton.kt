package sk.app.mhdlm.components

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import sk.app.mhdlm.R
import sk.app.mhdlm.data.DepartureData
import sk.app.mhdlm.data.SharedViewModel
import sk.app.mhdlm.screens.navigation.Screen

/**
 * Search button post all data to shared view model
 * for accessing data in screen with results.
 *
 * @author Pavol Tomčik
 */

@Composable
fun SearchButton(
    navController: NavController,
    sharedViewModel: SharedViewModel,
    from: String,
    to: String,
    isArrival:Boolean,
    departureDataList: List<DepartureData>
) {
    Row(
        modifier = Modifier
            .fillMaxWidth(),
        verticalAlignment = Alignment.Top,
    ) {
        Button(
            onClick = {
                sharedViewModel.startBusStop = from
                sharedViewModel.finalBusStop = to
                sharedViewModel.isArrival = isArrival
                sharedViewModel.departureDataList = departureDataList

                sharedViewModel.saveLastBusStops()
                navController.navigate(route = Screen.Results.route)
            },
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Text(text = stringResource(R.string.search))
        }
    }
}