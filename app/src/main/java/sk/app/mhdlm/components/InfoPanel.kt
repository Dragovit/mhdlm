package sk.app.mhdlm.components

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import sk.app.mhdlm.R
import sk.app.mhdlm.logic.SearchLogic
import sk.app.mhdlm.data.SharedViewModel as SharedViewModel1

/**
 * Info panel shows time of departure bus from bus stop according to current time and date.
 *
 * @author Pavol Tomčik
 */

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun InfoPanel(
    sharedViewModel: SharedViewModel1,
    departuresList: List<String>,
    index: Int,
    borderPadding: Dp,
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = MaterialTheme.colorScheme.primary)
    ) {
        LaunchedEffect(Unit) {
            sharedViewModel.timeRefresh()
        }

        if (sharedViewModel.date == sharedViewModel.currentDate) {

            val minutes = SearchLogic.parseTime(departuresList[index]) - SearchLogic.parseTime(sharedViewModel.currentTime)

            if (minutes > 60) {
                val (hour, minute) = SearchLogic.convertMinutesToHoursAndMinutes(minutes)
                Text(
                    text = stringResource(R.string.departureTimeInfoHours, hour, minute),
                    color = MaterialTheme.colorScheme.background,
                    modifier = Modifier.padding(start = borderPadding)
                )
            } else if(minutes > 0) {
                Text(
                    text = stringResource(R.string.departureTimeInfoMinutes, minutes),
                    color = MaterialTheme.colorScheme.background,
                    modifier = Modifier.padding(start = borderPadding)
                )
            } else {
                Text(
                    text = stringResource(R.string.today, sharedViewModel.time),
                    color = MaterialTheme.colorScheme.background,
                    modifier = Modifier.padding(start = borderPadding)
                )
            }

        } else {
            Text(
                text = stringResource(R.string.departureDateTimeInfo, sharedViewModel.date, sharedViewModel.time),
                color = MaterialTheme.colorScheme.background,
                modifier = Modifier.padding(start = borderPadding)
            )
        }

    }
}