package sk.app.mhdlm.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import sk.app.mhdlm.data.DepartureData

/**
 * Shows time and bus stop in results screen
 * according to criteria form main screen
 *
 * @author Pavol Tomčik
 */

@Composable
fun TimeAndBusStop(
    busStop: String,
    departureDataList: List<DepartureData>,
    timeList: List<String>,
    index: Int,
    borderPadding: Dp
) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .fillMaxWidth()
            .padding(end = borderPadding)
    ) {
        Text(
            text = "${timeList[index]} $busStop",
            modifier = Modifier
                .padding(start = borderPadding)
                .weight(1f)
        )
        IconRequestStop(
            busStopName = busStop,
            departureDataList = departureDataList
        )
    }
}