package sk.app.mhdlm.components

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import sk.app.mhdlm.data.DepartureData

/**
 * DropDownList shows bus stops when the user types in the text field.
 * Searches for bus stops according to text in text field.
 * If bus stop does exist then will be shown and is possible to click on the stop
 * for filling text field with that bus stop.
 *
 * @author Pavol Tomčik
 */

@Composable
fun DropDownList(
    isExpanded: Boolean,
    departureDataList: List<DepartureData>,
    onItemClick: (DepartureData) -> Unit
) {
    AnimatedVisibility(visible = isExpanded) {
        Card(
            modifier = Modifier
                .padding(5.dp)
                .fillMaxWidth(),
            elevation = CardDefaults.elevatedCardElevation(10.dp)
        ) {
            LazyColumn(
                modifier = Modifier
                    .heightIn(max = 150.dp)
                    .fillMaxWidth()
            ) {
                items(departureDataList) { departure ->
                    TextButton(onClick = { onItemClick(departure) }) {
                        Text(
                            text = departure.busStopName,
                            modifier = Modifier.fillMaxWidth()
                        )
                    }
                }
            }
        }
    }
}