package sk.app.mhdlm.logic

import android.os.Build
import androidx.annotation.RequiresApi
import sk.app.mhdlm.data.DepartureData
import sk.app.mhdlm.data.Symbols
import java.time.DayOfWeek
import java.time.LocalDate

/**
 * Class for searching logic
 *
 * @param startBusStop The started bus stop name
 * @param finalBusStop The final bus stop name
 * @param time The time in HH:mm format
 * @param date The date in dd.MM.yyyy format
 * @param departureDataList The list of data. It contain bus stop name and list of departures
 *
 * @author Pavol Tomčik
 */
class SearchLogic(
    private val startBusStop: String,
    private val finalBusStop: String,
    private val time: String,
    private val date: String,
    private val isArrival: Boolean,
    private val departureDataList: List<DepartureData>

) {

    companion object {
        /**
         * Parsing time and calculate to minutes
         *
         * @param timeStr The time in HH:mm format
         *
         * @return The parsed time in minutes
         */
        @RequiresApi(Build.VERSION_CODES.O)
        fun parseTime(timeStr: String): Int {
            val partsList = timeStr.split(":")
            if (partsList.size == 2) {

                // if item in partsList is not a number then store 0
                val hours = partsList[0].toIntOrNull() ?: 0
                val minutes = partsList[1].toIntOrNull() ?: 0

                // Calculate to minutes
                return hours * 60 + minutes

            }
            return 0
        }

        fun convertMinutesToHoursAndMinutes(minutes: Int): Pair<Int, Int> {
            // Calculate the number of hours
            val hours = minutes / 60

            // Calculate the remaining minutes after extracting hours
            val remainingMinutes = minutes % 60

            // Return the result as a Pair of hours and remaining minutes
            return Pair(hours, remainingMinutes)
        }
    }

    /**
     * Sorting bus connections for displaying on screen
     *
     * @return The three sorted lists in triple. The first list is departures list,
     *         the second list is arrivals list and the last is bus number list
     */
    @RequiresApi(Build.VERSION_CODES.O)
    fun sorter(

    ): Triple<List<String>, List<String>, List<String>> {
        val (departuresList, arrivalsList, busNumberList) = searchBusConnections()

        val departuresListSorted = departuresList.sortedBy { parseTime(it) }
        val arrivalsListSorted = arrivalsList.sortedBy { parseTime(it) }

        val zippedList = departuresList.zip(busNumberList)

        val sortedZippedList = zippedList.sortedBy { parseTime(it.first) }
        val busNumberListSorted = sortedZippedList.map { it.second }

        return Triple(departuresListSorted, arrivalsListSorted, busNumberListSorted)
    }


    /**
     * Searching bus connections from data class
     *
     * @return The three lists in triple. The first list is departures list,
     *         the second list is arrivals list and the last is bus number list
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun searchBusConnections(): Triple<MutableList<String>, MutableList<String>, MutableList<String>> {

        // Search bus stops
        val startDestination = departureDataList.find { it.busStopName.equals(startBusStop, ignoreCase = true) }
        val finalDestination = departureDataList.find { it.busStopName.equals(finalBusStop, ignoreCase = true) }

        val departuresList = mutableListOf<String>()
        val arrivalsList = mutableListOf<String>()
        val busNumberList = mutableListOf<String>()

        val startDestinationIndex = departureDataList.indexOf(startDestination)

        if (startDestination != null && finalDestination != null) {
            // Convert time to minutes for comparison
            val timeInMinutes = parseTime(time)

            for ((index, timeData) in startDestination.timeList.withIndex()) {
                val timeDataInMinutes = parseTime(timeData)

                val symbol = startDestination.symbolsList[index]
                val isValidDate = checkSymbols(date, symbol)

                val timeForArrival = if (isArrival) 60 else 0

                if (isValidDate && timeDataInMinutes >= (timeInMinutes - timeForArrival) && index < startDestination.timeList.size) {
                    if (parseTime(startDestination.timeList[index]) < parseTime(finalDestination.timeList[index])) {
                        departuresList.add(timeData)
                        arrivalsList.add(finalDestination.timeList[index])
                        busNumberList.add(departureDataList[startDestinationIndex].busNumbersList[index])
                    }
                }
            }
        }

        return Triple(departuresList, arrivalsList, busNumberList)
    }

    /**
     * Checking if date match symbol
     *
     * @param date
     * @param symbol
     *
     * @return true or false
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun checkSymbols(date: String, symbol: String): Boolean {

        val (day, month, year) = date.split(".").map { it.toInt() }
        val dateToCheck = LocalDate.of(year, month, day)

        // Check the symbol
        return when (symbol) {
            Symbols.SYMBOL_WORK_DAYS.value -> symbolWorkDays(dateToCheck)
            Symbols.SYMBOL_HOLIDAYS.value -> symbolHolidays(dateToCheck)
            Symbols.SYMBOL_6.value -> symbol6(dateToCheck)
            Symbols.SYMBOL_10.value -> symbol10(dateToCheck)
            Symbols.SYMBOL_11.value -> symbol11(dateToCheck)
            Symbols.SYMBOL_20.value -> symbol20(dateToCheck)
            Symbols.SYMBOL_27.value -> symbol27(dateToCheck)
            Symbols.SYMBOL_38.value -> symbol38(dateToCheck)
            Symbols.SYMBOL_47.value -> symbol47(dateToCheck)
            Symbols.SYMBOL_56.value -> symbol56(dateToCheck)
            Symbols.SYMBOL_97.value -> symbol97(dateToCheck)

            Symbols.SYMBOL_6_HOLIDAYS.value -> symbol6(dateToCheck) || symbolHolidays(dateToCheck)
            Symbols.SYMBOL_6_20.value -> symbol6(dateToCheck) && symbol20(dateToCheck)
            Symbols.SYMBOL_6_HOLIDAYS_27.value -> (symbol6(dateToCheck) || symbolHolidays(dateToCheck)) && symbol27(dateToCheck)
            Symbols.SYMBOL_6_HOLIDAYS_38.value -> (symbol6(dateToCheck) || symbolHolidays(dateToCheck)) && symbol38(dateToCheck)
            Symbols.SYMBOL_6_HOLIDAYS_47.value -> (symbol6(dateToCheck) || symbolHolidays(dateToCheck)) && symbol47(dateToCheck)
            Symbols.SYMBOL_6_HOLIDAYS_56.value -> (symbol6(dateToCheck) || symbolHolidays(dateToCheck)) && symbol56(dateToCheck)

            Symbols.SYMBOL_WORK_DAYS_HOLIDAY.value -> symbolWorkDays(dateToCheck) || symbolHolidays(dateToCheck)
            Symbols.SYMBOL_WORK_DAYS_6.value -> symbolWorkDays(dateToCheck) || symbol6(dateToCheck)
            Symbols.SYMBOL_WORK_DAYS_6_20.value -> (symbolWorkDays(dateToCheck) || symbol6(dateToCheck)) && symbol20(dateToCheck)
            Symbols.SYMBOL_WORK_DAYS_10.value -> symbolWorkDays(dateToCheck) && symbol10(dateToCheck)
            Symbols.SYMBOL_WORK_DAYS_11.value -> symbolWorkDays(dateToCheck) && symbol11(dateToCheck)
            Symbols.SYMBOL_WORK_DAYS_27.value -> symbolWorkDays(dateToCheck) && symbol27(dateToCheck)
            Symbols.SYMBOL_WORK_DAYS_56.value -> symbolWorkDays(dateToCheck) && symbol56(dateToCheck)

            else -> true // Default case for unknown symbols
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun symbolWorkDays(date: LocalDate): Boolean {
        return date.dayOfWeek != DayOfWeek.SATURDAY && date.dayOfWeek != DayOfWeek.SUNDAY
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun symbolHolidays(date: LocalDate): Boolean {
        return date.dayOfWeek == DayOfWeek.SUNDAY || holidays(date)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun symbol6(date: LocalDate): Boolean {
        return date.dayOfWeek == DayOfWeek.SATURDAY
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun symbol10(date: LocalDate): Boolean {
        return !symbol11(date)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun symbol11(date: LocalDate): Boolean {
        return date.isAfter(LocalDate.of(date.year - 1, 12, 27)) &&
                date.isBefore(LocalDate.of(date.year, 1, 5)) &&

                date.isAfter(LocalDate.of(date.year, 2, 19)) &&
                date.isBefore(LocalDate.of(date.year, 2, 23)) &&

                date.isAfter(LocalDate.of(date.year, 3, 28)) &&
                date.isBefore(LocalDate.of(date.year, 4, 2)) &&

                date.isAfter(LocalDate.of(date.year, 7, 1)) &&
                date.isBefore(LocalDate.of(date.year, 8, 30)) &&

                date.isAfter(LocalDate.of(date.year, 10, 30)) &&
                date.isBefore(LocalDate.of(date.year, 10, 31))
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun symbol20(date: LocalDate): Boolean {
        return date != LocalDate.of(date.year, 1, 6)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun symbol27(date: LocalDate): Boolean {
        return !symbol56(date)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun symbol38(date: LocalDate): Boolean {
        return date != LocalDate.of(date.year - 1, 12, 24)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun symbol47(date: LocalDate): Boolean {
        return symbol38(date) && date != LocalDate.of(date.year - 1, 12, 31)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun symbol56(date: LocalDate): Boolean {
        return date.isAfter(LocalDate.of(date.year, 7, 1)) &&
                date.isBefore(LocalDate.of(date.year, 8, 31))
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun symbol97(date: LocalDate): Boolean {
        return !date.isAfter(LocalDate.of(date.year - 1, 12, 10)) &&
                !date.isBefore(LocalDate.of(date.year, 12, 14))
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun holidays(date: LocalDate): Boolean {
        return date == LocalDate.of(date.year, 1, 1) &&
                date == LocalDate.of(date.year, 7, 5) &&
                date == LocalDate.of(date.year, 8, 29) &&
                date == LocalDate.of(date.year, 9, 1) &&
                date == LocalDate.of(date.year, 11, 17) &&

                date == LocalDate.of(date.year, 1, 6) &&
                date == LocalDate.of(date.year, 5, 1) &&
                date == LocalDate.of(date.year, 5, 8) &&
                date == LocalDate.of(date.year, 9, 15) &&
                date == LocalDate.of(date.year, 11, 1) &&
                date == LocalDate.of(date.year, 12, 24) &&
                date == LocalDate.of(date.year, 12, 25) &&
                date == LocalDate.of(date.year, 12, 26)
    }
}