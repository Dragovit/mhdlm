package sk.app.mhdlm

import android.content.Context
import sk.app.mhdlm.data.DepartureData
import java.io.BufferedReader
import java.io.InputStreamReader

/**
 * Reading csv file and store data to data class
 *
 * @param context The context of application
 *
 * @author Pavol Tomčik
 */
class CsvFileReader(private val context: Context) {
    /**
     * Function for reading csv file and store data to data class
     *
     * @param fileName The name of csv file
     *
     * @return departureData The list of data sorted by bus stop names
     */
    fun read(fileName: String): List<DepartureData> {

        // Accessing to assets (directory for csv file)
        val assetManager = context.resources.assets
        val inputStream = assetManager.open(fileName)


        val departureData = mutableListOf<DepartureData>()

        var symbolsList: List<String> = emptyList()
        var busNumbersList: List<String> = emptyList()

        // Reading csv file line by line and parsing data by ";"
        BufferedReader(InputStreamReader(inputStream)).useLines { lines ->
            var LINE = 1
            lines.forEach { line ->
                val data = line.split(";")
                if (data.isNotEmpty()) {

                    if (LINE == 1) {
                        symbolsList = data.subList(2, data.size)
                        LINE = 2
                    }
                    else if (LINE == 2) {
                        busNumbersList = data.subList(2, data.size)
                        LINE = 0
                    }

                    val requestStop = data[0]
                    val busStop = data[1]
                    val timesList = data.subList(2, data.size)

                    departureData.add(DepartureData(busStop, timesList, busNumbersList, symbolsList, requestStop))
                }
            }
        }

        return departureData.sortedBy { it.busStopName }
    }
}