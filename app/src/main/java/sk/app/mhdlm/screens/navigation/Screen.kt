package sk.app.mhdlm.screens.navigation

/**
 * Route to the screens
 *
 * @author Pavol Tomčik
 */

sealed class Screen(val route: String) {
    data object Main: Screen(route = "main_screen")
    data object Results: Screen(route = "results_screen")
}