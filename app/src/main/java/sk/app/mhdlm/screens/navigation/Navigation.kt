package sk.app.mhdlm.screens.navigation

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import sk.app.mhdlm.data.SharedViewModel
import sk.app.mhdlm.screens.MainScreen
import sk.app.mhdlm.screens.ResultsScreen

/**
 * Navigation between screens
 *
 * @author Pavol Tomčik
 */

class Navigation {
    @RequiresApi(Build.VERSION_CODES.O)
    @Composable
    fun SetUp(navController: NavHostController, sharedViewModel: SharedViewModel) {
        NavHost(
            navController = navController,
            startDestination = Screen.Main.route
        ) {
            composable(
                route = Screen.Main.route
            ) {
                MainScreen(navController = navController, sharedViewModel = sharedViewModel)
            }
            composable(
                route = Screen.Results.route
            ) {
                ResultsScreen(navController = navController, sharedViewModel = sharedViewModel)
            }
        }
    }
}