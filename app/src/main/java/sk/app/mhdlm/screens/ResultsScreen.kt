package sk.app.mhdlm.screens

import android.app.Application
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import sk.app.mhdlm.R
import sk.app.mhdlm.components.InfoPanel
import sk.app.mhdlm.components.TimeAndBusStop
import sk.app.mhdlm.data.SharedViewModel
import sk.app.mhdlm.logic.SearchLogic
import sk.app.mhdlm.ui.theme.MHDLMTheme

/**
 * Result screen showing results from main screen
 *
 * @author Pavol Tomčik
 */

@RequiresApi(Build.VERSION_CODES.O)
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ResultsScreen(navController: NavController, sharedViewModel: SharedViewModel, modifier: Modifier = Modifier) {
    Scaffold(
        modifier = modifier.fillMaxSize(),
        topBar = {
            TopAppBar(
                title = {Text(text = stringResource(R.string.results))},
                navigationIcon = {
                    IconButton(onClick = {
                        navController.popBackStack()
                    }) {
                        Icon(
                            imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                            contentDescription = stringResource(R.string.arrowBack),
                            tint = MaterialTheme.colorScheme.background
                        )
                    }
                },
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primary,
                    titleContentColor = MaterialTheme.colorScheme.background

                )
            )
        },
    ) { values ->

        ShowResults(values = values, sharedViewModel = sharedViewModel)

    }
}


@RequiresApi(Build.VERSION_CODES.O)
@Composable
private fun ShowResults(values: PaddingValues, sharedViewModel: SharedViewModel, modifier: Modifier = Modifier) {

    val (departuresList, arrivalsList, busNumbersList) = SearchLogic(
        startBusStop = sharedViewModel.startBusStop,
        finalBusStop = sharedViewModel.finalBusStop,
        time = sharedViewModel.time,
        date = sharedViewModel.date,
        isArrival = sharedViewModel.isArrival,
        departureDataList = sharedViewModel.departureDataList
    ).sorter()


    if (departuresList.isNotEmpty() && departuresList.size == arrivalsList.size) {

        val borderPadding = 16.dp
        val spacerHeight = 8.dp

        LazyColumn(
            modifier = modifier
                .fillMaxSize()
                .padding(values),
            verticalArrangement = Arrangement.Top
        ) {
            item {

                departuresList.indices.forEach {
                    Card(
                        modifier = modifier
                            .fillMaxSize()
                            .padding(borderPadding)
                    ) {
                        Column {
                            InfoPanel(
                                sharedViewModel = sharedViewModel,
                                departuresList = departuresList,
                                index = it,
                                borderPadding = borderPadding
                            )
                            Row(
                                verticalAlignment = Alignment.CenterVertically,
                                horizontalArrangement = Arrangement.Start,
                                modifier = modifier
                                    .fillMaxWidth()
                                    .padding(start = borderPadding)

                            ) {
                                Icon(
                                    painter = painterResource(R.drawable.bus),
                                    contentDescription = stringResource(R.string.busIconDes)
                                )
                                Text(text = busNumbersList[it])
                                Column {
                                    Spacer(modifier = modifier.height(spacerHeight))
                                    TimeAndBusStop(
                                        busStop = sharedViewModel.startBusStop,
                                        departureDataList = sharedViewModel.departureDataList,
                                        timeList = departuresList,
                                        index = it,
                                        borderPadding = borderPadding
                                    )
                                    TimeAndBusStop(
                                        busStop = sharedViewModel.finalBusStop,
                                        departureDataList = sharedViewModel.departureDataList,
                                        timeList = arrivalsList,
                                        index = it,
                                        borderPadding = borderPadding
                                    )
                                    Spacer(modifier = modifier.height(spacerHeight))
                                }
                            }
                        }
                    }

                }

            }

        }
    } else {

        Column(
            modifier = modifier
                .fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(text = stringResource(R.string.NoBusLink))
        }
    }

}


@RequiresApi(Build.VERSION_CODES.O)
@Preview(showBackground = true)
@Composable
fun ResultsScreenPreview() {
    MHDLMTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            ResultsScreen(navController = rememberNavController(), sharedViewModel = SharedViewModel(Application()))
        }
    }
}