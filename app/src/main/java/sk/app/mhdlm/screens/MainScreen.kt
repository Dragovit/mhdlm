package sk.app.mhdlm.screens

import android.app.Application
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Clear
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import sk.app.mhdlm.CsvFileReader
import sk.app.mhdlm.R
import sk.app.mhdlm.components.ArrivalSwitch
import sk.app.mhdlm.components.DateTime
import sk.app.mhdlm.components.DropDownList
import sk.app.mhdlm.components.EditTextField
import sk.app.mhdlm.components.LastLinksCards
import sk.app.mhdlm.components.SearchButton
import sk.app.mhdlm.data.SharedViewModel
import sk.app.mhdlm.ui.theme.MHDLMTheme

/**
 * Main screen is for reading data from user
 *
 * @author Pavol Tomčik
 */

@RequiresApi(Build.VERSION_CODES.O)
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScreen(navController: NavController, sharedViewModel: SharedViewModel, modifier: Modifier = Modifier) {
    var fromText by rememberSaveable { mutableStateOf(String()) }
    var toText by rememberSaveable { mutableStateOf(String()) }
    var isArrival by rememberSaveable { mutableStateOf(false) }

    var expandedFrom by remember { mutableStateOf(false) }
    var expandedTo by remember { mutableStateOf(false) }

    // Reading csv file form assets folder
    val context = LocalContext.current
    val csvFileReader = remember { CsvFileReader(context) }
    val departureDataList by remember {
        mutableStateOf(csvFileReader.read("Odchody.csv"))
    }


    Scaffold(
        topBar = {
            CenterAlignedTopAppBar(
                title = {
                    Text(text = stringResource(R.string.app_name))
                },
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primary,
                    titleContentColor = MaterialTheme.colorScheme.background
                )
            )
        },
        modifier = modifier
    ) {value ->
        Row {
            IconButton(
                onClick = {

                    val helpValue = fromText
                    fromText = toText
                    toText = helpValue

                          },
                modifier = modifier
                    .padding(value)
                    .padding(top = 145.dp)) {
                Icon(
                    painter = painterResource(R.drawable.swap),
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.primary
                )
            }

            Column(
                modifier = modifier
                    .fillMaxSize()
                    .padding(value)
                    .padding(end = 40.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Spacer(modifier = modifier.padding(50.dp))

                EditTextField(
                    label = R.string.fromLabel,
                    value = fromText,
                    onValueChange = {
                        fromText = it
                        expandedFrom = true
                    },
                    leadingIcon = R.drawable.location,
                    contentDes = R.string.locationFromIconDes,
                    trailingIcon = {
                        IconButton(onClick = {
                            expandedFrom = !expandedFrom
                            fromText = ""}) {
                            Icon(
                                imageVector = Icons.Rounded.Clear,
                                contentDescription = stringResource(R.string.clearDes)
                            )
                        }
                    },
                    keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next)
                )

                DropDownList(
                    isExpanded = expandedFrom && fromText.isNotBlank(),
                    departureDataList = departureDataList.filter {
                        it.busStopName.lowercase().contains(fromText.lowercase())
                    },
                    onItemClick = { selectedBusStop ->
                        fromText = selectedBusStop.busStopName
                        expandedFrom = false
                    }
                )

                EditTextField(
                    label = R.string.toLabel,
                    value = toText,
                    onValueChange = {
                        toText = it
                        expandedTo = true
                    },
                    leadingIcon = R.drawable.flag,
                    contentDes = R.string.locationToIconDes,
                    trailingIcon = {
                        IconButton(onClick = {
                            expandedTo = !expandedTo
                            toText = ""
                        }) {
                            Icon(
                                imageVector = Icons.Rounded.Clear,
                                contentDescription = stringResource(R.string.clearDes)
                            )
                        }
                    },
                    keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done)
                )

                DropDownList(
                    isExpanded = expandedTo && toText.isNotBlank(),
                    departureDataList = departureDataList.filter {
                        it.busStopName.lowercase().contains(toText.lowercase())
                    },
                    onItemClick = {
                            selectedBusStop ->
                        toText = selectedBusStop.busStopName
                        expandedTo = false
                    }
                )

                DateTime(sharedViewModel)
                ArrivalSwitch(
                    isArrival = isArrival,
                    onCheckedChange = {isArrival = it}
                )
                Spacer(modifier = modifier.padding(top = 16.dp))
                SearchButton(
                    navController = navController,
                    sharedViewModel = sharedViewModel,
                    from = fromText,
                    to = toText,
                    isArrival = isArrival,
                    departureDataList = departureDataList
                )
                LastLinksCards(
                    navController = navController,
                    sharedViewModel = sharedViewModel,
                    departureDataList = departureDataList,
                    isArrival = isArrival
                )

            }
        }
        

    }
}



@RequiresApi(Build.VERSION_CODES.O)
@Preview(showBackground = true)
@Composable
fun MainScreenPreview() {
    MHDLMTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            MainScreen(navController = rememberNavController(), sharedViewModel = SharedViewModel(Application()))
        }
    }
}